import useSWR from 'swr';

import { Manifest } from '../types/Manifest';

function fetchManifest(url: string): Promise<Manifest> {
  return fetch(url)
    .then(res => res.json());
}

export default function useManifest(url) {
  return useSWR(url, fetchManifest);
}

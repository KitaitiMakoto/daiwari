import { Dispatch, SetStateAction } from 'react';

interface Args {
  setList: Dispatch<SetStateAction<any>>;
  from: number;
  to: number;
  area: 'upper' | 'downer'
}

export default function reorderList({setList, from, to, area}: Args) {
  if (area === 'upper') {
    if (from < to) {
      setList(pages => [
        ...pages.slice(0, from),
        ...pages.slice(from + 1, to),
        pages[from],
        ...pages.slice(to)
      ]);
    } else {
      setList(pages => [
        ...pages.slice(0, to),
        pages[from],
        ...pages.slice(to, from),
        ...pages.slice(from + 1)
      ]);
    }
  } else {
    if (from < to) {
      setList(pages => [
        ...pages.slice(0, from),
        ...pages.slice(from + 1, to + 1),
        pages[from],
        ...pages.slice(to + 1)
      ]);
    } else {
      setList(pages => [
        ...pages.slice(0, to + 1),
        pages[from],
        ...pages.slice(to + 1, from),
        ...pages.slice(from + 1)
      ]);
    }
  }
}

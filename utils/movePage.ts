import { Dispatch, SetStateAction } from 'react';

interface Args<T> {
  setFromList: Dispatch<SetStateAction<T[]>>;
  fromList: T[];
  fromIndex: number;
  setToList: Dispatch<SetStateAction<T[]>>;
  toIndex: number;
  area: 'upper' | 'downer';
}

export default function movePage<T>({
  setFromList,
  fromList,
  fromIndex,
  setToList,
  toIndex,
  area
}: Args<T>) {
  const fix = area === 'upper' ? 0 : 1;
  setFromList(pages => [
    ...pages.slice(0, fromIndex),
    ...pages.slice(fromIndex + 1)
  ]);
  setToList(pages => [
    ...pages.slice(0, toIndex + fix),
    fromList[fromIndex],
    ...pages.slice(toIndex + fix)
  ]);
}

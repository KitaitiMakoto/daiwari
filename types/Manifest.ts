export type Manifest = {
  metadata: Metadata;
  readingOrder: Resource[];
};

export type Metadata = {
  title: string;
};
  
export type Resource = {
  id?: string;
  text?: string;
  href?: string;
  width: number;
  height: number;
};

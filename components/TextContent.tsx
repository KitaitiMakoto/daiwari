import { ChangeEventHandler } from 'react';
import { Flex, Textarea } from '@chakra-ui/react';

type Props = {
  value?: string;
  aspectRatio?: string;
  onChange: ChangeEventHandler<HTMLTextAreaElement>;
};

export default function TextContent({
  value,
  aspectRatio,
  onChange
}: Props) {
  return (
    <Flex
      justify="center"
      align="center"
      style={{aspectRatio}}
      p={4}
      border="solid"
      bgColor="white"
    >
      <Textarea
        w="100%"
        h="100%"
        fontSize="2rem"
        style={{direction: 'ltr'}}
        value={value}
        placeholder="テキストを入力"
        onChange={onChange}
      />
    </Flex>
  );
}

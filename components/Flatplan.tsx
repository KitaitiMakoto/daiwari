import { useCallback, Dispatch, SetStateAction } from 'react';
import { IconButton, Box, Grid } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { useDrop } from 'react-dnd';
import Image from 'next/image';

import { Resource } from '../types/Manifest';
import Page from './Page';
import NoPageContent from './NoPageContent';
import TextContent from './TextContent';

type Props = {
  pages: Resource[];
  numColumns: number;
  numRows: number;
  direction?: 'rtl' | 'ltr';
  addPage: (index: number) => void;
  movePageAside: (index: number) => void;
  setReadingOrder: Dispatch<SetStateAction<Resource[]>>;
  floatingPages: Resource[];
  setFloatingPages: Dispatch<SetStateAction<Resource[]>>;
};

type Item = {
  index: number;
  listType: 'Flatplan' | 'FloatingPages';
};

export default function Flatplan({
  pages,
  numColumns,
  direction = 'rtl',
  addPage,
  movePageAside,
  setReadingOrder,
  floatingPages,
  setFloatingPages
}: Props): JSX.Element {
  const [_, drop] = useDrop({
    accept: 'Page',
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    }),
    drop: (item: Item, monitor) => {
      switch (item.listType) {
        case 'Flatplan':
          // Noop
          break;
        case 'FloatingPages':
          if (pages.length === 0) {
            setReadingOrder([floatingPages[item.index]]);
            setFloatingPages(prev => [
              ...prev.slice(0, item.index),
              ...prev.slice(item.index + 1)
            ]);
          } else {
            // Noop
            // <Page> handles the case
          }
          break;
      }
    }
  });

  const setTextValue = useCallback((index, text) => setReadingOrder([
    ...pages.slice(0, index),
    { ...pages[index], text },
    ...pages.slice(index + 1)
  ]), [pages]);

  return (
    <Box
      ref={drop}
      width="50vw"
      height="80vh"
      overflowY="auto"
    >
      <Box textAlign="center">
        <IconButton aria-label="ページを追加" icon={<AddIcon />} onClick={() => addPage(-1)} />
      </Box>
      <Grid
        templateColumns={`repeat(${numColumns}, 1fr)`}
        rowGap={12}
        overflowX="visible"
        px={6}
        style={{direction}}
      >
        {pages.map((page, index) => {
          const colIndex = index % numColumns;

          return page ?
            <Page
              key={page?.href ?? page?.id}
              index={index}
              listType="Flatplan"
              justifyNombre={colIndex === 0 ? 'flex-start' : colIndex === numColumns - 1 ? 'flex-end' : 'center'}
              addPage={() => addPage(index)}
              movePageAside={() => movePageAside(index)}
              readingOrder={pages}
              setReadingOrder={setReadingOrder}
              floatingPages={floatingPages}
              setFloatingPages={setFloatingPages}
            >
              {page.href ?
              <Image
                src={page.href}
                width={page.width}
                height={page.height}
                layout="responsive"
              /> :
              <TextContent
                value={page.text}
                aspectRatio={`${page.width} / ${page.height}`}
                onChange={(event) => setTextValue(index, event.target.value)}
              />
              }
            </Page> :
            <Page
              key={index}
              index={index}
              listType="Flatplan"
              justifyNombre={colIndex === 0 ? 'flex-start' : colIndex === numColumns - 1 ? 'flex-end' : 'center'}
              readingOrder={pages}
              setReadingOrder={setReadingOrder}
              floatingPages={floatingPages}
              setFloatingPages={setFloatingPages}
            >
              <NoPageContent aspectRatio={`${pages[0].width} / ${pages[0].height}`} />
            </Page>;
          })}
      </Grid>
    </Box>
  );
}

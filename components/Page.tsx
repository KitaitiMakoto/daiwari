import { ReactNode, useState, useRef, Dispatch, SetStateAction } from 'react';
import { IconButton, Box, Flex } from '@chakra-ui/react';
import { AddIcon, MinusIcon, DeleteIcon } from '@chakra-ui/icons';
import { useDrag, useDrop } from 'react-dnd';

import { Resource } from '../types/Manifest';
import reorderList from '../utils/reorderList';
import movePage from '../utils/movePage';

type Props = {
  index: number;
  listType: 'Flatplan' | 'FloatingPages';
  justifyNombre?: 'flex-start' | 'flex-end' | 'center';
  addPage?: () => void;
  movePageAside?: () => void;
  deletePage?: () => void;
  readingOrder: Resource[];
  setReadingOrder: Dispatch<SetStateAction<Resource[]>>;
  floatingPages: Resource[];
  setFloatingPages: Dispatch<SetStateAction<Resource[]>>;
  children?: ReactNode;
};

type Item = {
  index: number;
  listType: 'Flatplan' | 'FloatingPages'
};

export default function Page({
  index,
  listType,
  justifyNombre,
  addPage,
  movePageAside,
  deletePage,
  readingOrder,
  setReadingOrder,
  floatingPages,
  setFloatingPages,
  children
}: Props) {
  const ref = useRef(null);
  const [focused, setFocused] = useState(false);
  const [droppingArea, setDroppingArea] = useState<'upper' | 'downer' | undefined>(undefined);
  const [{ isDragging }, drag] = useDrag({
    type: 'Page',
    item: { index, listType },
    collect: monitor => ({ isDragging: monitor.isDragging() })
  });
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'Page',
    collect: (monitor) => ({
      canDrop: monitor.canDrop(),
      isOver: monitor.isOver()
    }),
    drop: (item: Item, monitor) => {
      setDroppingArea(undefined);
      if (! ref.current) {
        return;
      }
      if (item.listType === listType && item.index === index) {
        return;
      }
      console.debug('TODO: Consider direction: rtl or ltr');
      const hoverBoundingRect = ref.current.getBoundingClientRect();
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      const area = hoverClientY < hoverMiddleY ? 'upper' : 'downer';
      const fix = area === 'upper' ? 0 : 1;
      switch (listType) {
        case 'Flatplan':
          switch (item.listType) {
            case 'Flatplan':
              reorderList({setList: setReadingOrder, from: item.index, to: index, area});
              break;
            case 'FloatingPages':
              movePage({
                setFromList: setFloatingPages,
                fromList: floatingPages,
                fromIndex: item.index,
                setToList: setReadingOrder,
                toIndex: index,
                area
              });
              break;
          }
          break;
        case 'FloatingPages':
          switch (item.listType) {
            case 'Flatplan':
              movePage({
                setFromList: setReadingOrder,
                fromList: readingOrder,
                fromIndex: item.index,
                setToList: setFloatingPages,
                toIndex: index,
                area
              });
              break;
            case 'FloatingPages':
              reorderList({setList: setFloatingPages, from: item.index, to: index, area});
              break;
          }
          break;
      }
    },
    hover: (item, monitor) => {
      if (item.index === index) {
        setDroppingArea(undefined);
        return;
      }
      if (! monitor.canDrop()) {
        setDroppingArea(undefined);
        return;
      }
      if (! monitor.isOver()) {
        setDroppingArea(undefined);
        return;
      }
      const hoverBoundingRect = ref.current.getBoundingClientRect();
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      const area = hoverClientY < hoverMiddleY ? 'upper' : 'downer';
      setDroppingArea(area);
    }
  });
  drag(drop(ref));

  const isNoPage = !(addPage || movePageAside || deletePage);

  if (droppingArea && !(canDrop && isOver)) {
    setDroppingArea(undefined);
  }

  return (
    <Box
      ref={isNoPage ? undefined : ref}
      w="100%"
      p={2}
      position="relative"
      tabIndex={isNoPage ? undefined : 0}
      _focusWithin={{
        backgroundColor: 'cornsilk',
        cursor: 'move'
      }}
      boxShadow={focused ? 'lg' : undefined}
      cursor={isNoPage ? 'auto' : 'pointer'}
      opacity={isDragging ? '0.5' : '1'}
      transform={droppingArea && (droppingArea === 'upper' ? 'translateY(5%)' : 'translateY(-5%)')}
      transition="transform 200ms"
      onFocus={() => setFocused(true)}
      onBlur={() => setFocused(false)}
    >
      {justifyNombre &&
        <Flex
          justify={justifyNombre}
          px={2}
          color="#555555"
          bgColor="#f8f8f8"
        >P. {index + 1}</Flex>}
      {children}
      {addPage &&
        <IconButton
          aria-label="このページの後にページを追加"
          position="absolute"
          insetInlineStart="0" /* endじゃないの??? */
          insetBlockEnd="50%"
          transform="translateX(-50%)"
          zIndex="docked"
          icon={<AddIcon />}
          onClick={addPage}
          opacity={focused ? '1' : '0'} /* FIXME */
        />}
      {movePageAside &&
        <IconButton
          aria-label="ページを脇に除ける"
          position="absolute"
          insetInlineEnd="0"
          insetBlockEnd="0"
          icon={<MinusIcon />}
          onClick={movePageAside}
          opacity={focused ? '1' : '0'}
        />}
      {deletePage &&
        <IconButton
          aria-label="ページを削除"
          position="absolute"
          insetInlineEnd="0"
          insetBlockEnd="0"
          icon={<DeleteIcon />}
          onClick={deletePage}
          opacity={focused ? '1' : '0'}
        />}
    </Box>
  );
}

import { Flex } from '@chakra-ui/react';

import Page from './Page';

type Props = {
  aspectRatio?: string;
};

export default function NoPageContent({ aspectRatio }: Props) {
  return (
    <Flex
      justify="center"
      align="center"
      style={{aspectRatio}}
      border="dashed 1px"
      bgColor="white"
    >
      No Page
    </Flex>
  );
}

import { useCallback, Dispatch, SetStateAction } from 'react';
import { chakra, IconButton, Box, Flex } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { useDrop } from 'react-dnd';
import Image from 'next/image';

import { Resource } from '../types/Manifest';
import Page from './Page';
import TextContent from './TextContent';

type Props = {
  pages: Resource[];
  addFloatingPage: () => void;
  deletePage: (index: number) => void;
  readingOrder: Resource[];
  setReadingOrder: Dispatch<SetStateAction<Resource[]>>;
  setFloatingPages: Dispatch<SetStateAction<Resource[]>>;
};

type Item = {
  index: number;
  listType: 'Flatplan' | 'FloatingPages'
};

export default function({
  pages,
  addFloatingPage,
  deletePage,
  readingOrder,
  setReadingOrder,
  setFloatingPages
}: Props) {
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'Page',
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    }),
    drop: (item: Item, monitor) => {
      switch (item.listType) {
        case 'Flatplan':
          if (pages.length === 0) {
            setReadingOrder(prev => [
              ...prev.slice(0, item.index),
              ...prev.slice(item.index + 1)
            ]);
            setFloatingPages([readingOrder[item.index]]);
          } else {
            // Noop
            // <Page> handles this case
          }
          break;
        case 'FloatingPages':
          // Noop
          break;
      }
    }
  });

  const setTextValue = useCallback((index, text) => setFloatingPages([
    ...pages.slice(0, index),
    { ...pages[index], text },
    ...pages.slice(index + 1)
  ]), [pages]);

  return (
    <Box
      ref={drop}
      position="relative"
      w="20vw"
      h="80vh"
      px="2"
      pb="2"
      overflowY="auto"
      border={canDrop && isOver ? 'dashed black' : 'dotted gray'}
      transition="border 300ms"
    >
      <chakra.h2 d="flex" justifyContent="center" alignItems="center" h="10">一時置き場</chakra.h2>
      <IconButton
        aria-label="ページを作成"
        icon={<AddIcon />}
        position="absolute"
        top="0"
        right="0"
        onClick={addFloatingPage}
        />
      {pages.map((page, index) => (
        <Page
          key={page.href ?? index}
          index={index}
          listType="FloatingPages"
          deletePage={() => deletePage(index)}
          readingOrder={readingOrder}
          setReadingOrder={setReadingOrder}
          floatingPages={pages}
          setFloatingPages={setFloatingPages}
        >
          {page.href ?
          <Image
            src={page.href}
            width={page.width}
            height={page.height}
            layout="responsive"
          /> :
          <TextContent
            value={page.text}
            aspectRatio={`${page.width} / ${page.height}`}
            onChange={(event) => setTextValue(index, event.target.value)}
          />}
        </Page>
      ))}
    </Box>
  );
}
